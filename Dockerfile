FROM ubuntu:20.04

ARG RABBITMQ_VERSION

RUN apt-get update -qq && apt-get install -qq --no-install-recommends curl python3 ca-certificates && \
  curl -fsSL https://raw.githubusercontent.com/rabbitmq/rabbitmq-management/v${RABBITMQ_VERSION}/bin/rabbitmqadmin -o /usr/local/bin/rabbitmqadmin && \
  chmod +x /usr/local/bin/rabbitmqadmin && \
  apt-get clean && rm -rf /var/lib/apt/lists/*

COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]
